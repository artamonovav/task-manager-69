//package ru.t1.artamonov.tm.service;
//
//import org.jetbrains.annotations.NotNull;
//import org.jetbrains.annotations.Nullable;
//import org.junit.Assert;
//import org.junit.BeforeClass;
//import org.junit.Test;
//import org.junit.experimental.categories.Category;
//import ru.t1.artamonov.tm.api.service.IConnectionService;
//import ru.t1.artamonov.tm.api.service.dto.IProjectDTOService;
//import ru.t1.artamonov.tm.api.service.dto.IProjectTaskDTOService;
//import ru.t1.artamonov.tm.api.service.dto.ITaskDTOService;
//import ru.t1.artamonov.tm.api.service.dto.IUserDTOService;
//import ru.t1.artamonov.tm.dto.model.TaskDTO;
//import ru.t1.artamonov.tm.marker.UnitCategory;
//import ru.t1.artamonov.tm.service.dto.ProjectDTOService;
//import ru.t1.artamonov.tm.service.dto.ProjectTaskDTOService;
//import ru.t1.artamonov.tm.service.dto.TaskDTOService;
//import ru.t1.artamonov.tm.service.dto.UserDTOService;
//
//import static ru.t1.artamonov.tm.constant.ProjectTestData.USER1_PROJECT1;
//import static ru.t1.artamonov.tm.constant.ProjectTestData.USER1_PROJECT2;
//import static ru.t1.artamonov.tm.constant.TaskTestData.USER1_TASK1;
//import static ru.t1.artamonov.tm.constant.TaskTestData.USER1_TASK2;
//import static ru.t1.artamonov.tm.constant.UserTestData.*;
//
//@Category(UnitCategory.class)
//public final class ProjectTaskServiceTest {
//
//    @Nullable
//    private static IProjectTaskDTOService projectTaskService;
//
//    @Nullable
//    private static IProjectDTOService projectService;
//
//    @Nullable
//    private static ITaskDTOService taskService;
//
//    @BeforeClass
//    public static void init() {
//        @NotNull PropertyService propertyService = new PropertyService();
//        @NotNull IConnectionService connectionService = new ConnectionService(propertyService);
//        @NotNull IUserDTOService userService = new UserDTOService(connectionService, propertyService);
//        projectTaskService = new ProjectTaskDTOService(connectionService);
//        projectService = new ProjectDTOService(connectionService);
//        taskService = new TaskDTOService(connectionService);
//        if (userService.findOneById(USER1.getId()) == null) userService.add(USER1);
//        if (userService.findOneById(USER2.getId()) == null) userService.add(USER2);
//        if (userService.findOneById(ADMIN.getId()) == null) userService.add(ADMIN);
//        projectService.add(USER1_PROJECT1);
//        projectService.add(USER1_PROJECT2);
//        taskService.add(USER1_TASK1);
//        taskService.add(USER1_TASK2);
//    }
//
//    @Test
//    public void bindTaskToProject() {
//        projectTaskService.bindTaskToProject(USER1.getId(), USER1_PROJECT1.getId(), USER1_TASK1.getId());
//        @NotNull TaskDTO task1 = taskService.findOneById(USER1.getId(), USER1_TASK1.getId());
//        Assert.assertEquals(USER1_PROJECT1.getId(), task1.getProjectId());
//        projectTaskService.bindTaskToProject(USER1.getId(), USER1_PROJECT2.getId(), USER1_TASK2.getId());
//        @NotNull TaskDTO task2 = taskService.findOneById(USER1.getId(), USER1_TASK2.getId());
//        Assert.assertEquals(USER1_PROJECT2.getId(), task2.getProjectId());
//    }
//
//    @Test
//    public void unbindTaskFromProject() {
//        bindTaskToProject();
//        @NotNull TaskDTO task1 = taskService.findOneById(USER1.getId(), USER1_TASK1.getId());
//        Assert.assertNotNull(task1.getProjectId());
//        projectTaskService.unbindTaskFromProject(USER1.getId(), USER1_PROJECT1.getId(), task1.getId());
//        @NotNull TaskDTO task2 = taskService.findOneById(USER1.getId(), USER1_TASK1.getId());
//        Assert.assertNull(task2.getProjectId());
//    }
//
//    @Test
//    public void removeProjectById() {
//        bindTaskToProject();
//        projectTaskService.removeProjectById(USER1.getId(), USER1_PROJECT1.getId());
//        Assert.assertFalse(projectService.existsById(USER1.getId(), USER1_PROJECT1.getId()));
//        Assert.assertFalse(taskService.existsById(USER1.getId(), USER1_TASK1.getId()));
//    }
//
//}
