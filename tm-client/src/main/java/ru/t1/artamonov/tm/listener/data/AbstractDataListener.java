package ru.t1.artamonov.tm.listener.data;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.t1.artamonov.tm.api.endpoint.IDomainEndpoint;
import ru.t1.artamonov.tm.enumerated.Role;
import ru.t1.artamonov.tm.listener.AbstractListener;

@Component
@NoArgsConstructor
public abstract class AbstractDataListener extends AbstractListener {

    @NotNull
    @Autowired
    protected IDomainEndpoint domainEndpointClient;

    @Override
    public @Nullable String getArgument() {
        return null;
    }

    @Override
    public @Nullable Role[] getRoles() {
        return new Role[]{Role.ADMIN};
    }

}
