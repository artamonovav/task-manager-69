package ru.t1.artamonov.tm.listener.system;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1.artamonov.tm.event.ConsoleEvent;
import ru.t1.artamonov.tm.listener.AbstractListener;

@Component
public final class CommandListListener extends AbstractSystemListener {

    @NotNull
    private static final String NAME = "commands";

    @NotNull
    private static final String ARGUMENT = "-cmd";

    @NotNull
    private static final String DESCRIPTION = "Show command list.";

    @NotNull
    @Override
    public String getName() {
        return "commands";
    }

    @NotNull
    @Override
    public String getArgument() {
        return "-cmd";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Display program commands.";
    }

    @Override
    @EventListener(condition = "@commandListListener.getName() == #consoleEvent.name")
    public void handler(@NotNull final ConsoleEvent consoleEvent) {
        System.out.println("[COMMANDS]");
        for (@NotNull final AbstractListener abstractListener : listeners) {
            @Nullable final String name = abstractListener.getName();
            if (name == null || name.isEmpty()) continue;
            System.out.println(name);
        }
    }

}
