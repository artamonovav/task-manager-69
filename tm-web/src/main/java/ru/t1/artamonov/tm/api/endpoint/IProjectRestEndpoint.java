package ru.t1.artamonov.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.web.bind.annotation.*;
import ru.t1.artamonov.tm.model.Project;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.Collection;

@WebService
@RequestMapping("api/projects")
public interface IProjectRestEndpoint {

    @WebMethod
    @PostMapping("/delete")
    void delete(
            @WebParam(name = "project", partName = "project")
            @RequestBody Project project
    );

    @WebMethod
    @PostMapping("/deleteById/{id}")
    void deleteById(
            @WebParam(name = "id", partName = "id")
            @PathVariable("id") String id
    );

    @Nullable
    @WebMethod
    @GetMapping("/findAll")
    Collection<Project> findAll();

    @Nullable
    @WebMethod
    @GetMapping("/findById/{id}")
    Project findById(
            @WebParam(name = "id", partName = "id")
            @PathVariable("id") String id
    );

    @NotNull
    @WebMethod
    @PostMapping("/save")
    Project save(
            @WebParam(name = "project", partName = "project")
            @RequestBody Project project
    );

}
